var exec = require('cordova/exec'),
    platform = require('cordova/platform');

var LibertyMe = function() {
};

// do nothing
function noop() {};

/**
 * Clear badge number (iOS)
 */
LibertyMe.prototype.clearBadgeNumber = function (success, fail) {
    success = success || noop;
    fail = fail || noop;

    if (platform.id == 'ios') {
        exec(success, fail, 'LibertyMe', 'clearBadgeNumber');
    } else {
        fail.call(window, platform.id + " is not supported!");
    }
};

/**
 * Copy to clipboard
 */
LibertyMe.prototype.copyToClipboard = function (content, success, fail) {
    success = success || noop;
    fail = fail || noop;

    if (!content) {
        fail.call(window, "Content is required!");
        return ;
    }

    exec(success, fail, 'LibertyMe', 'copyToClipboard', [content]);
};

// export it
module.exports = new LibertyMe();
