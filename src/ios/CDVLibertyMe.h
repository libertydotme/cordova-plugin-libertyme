//
//  CDVLibertyMe.h
//
//  Created by Xu Li on 2/22/15.
//
//

#import <Cordova/CDVPlugin.h>

@interface CDVLibertyMe : CDVPlugin

- (void)clearBadgeNumber:(CDVInvokedUrlCommand*)command;
- (void)copyToClipboard:(CDVInvokedUrlCommand*)command;

@end
