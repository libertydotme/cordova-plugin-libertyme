//
//  CDVLibertyMe.m
//
//  Created by Xu Li on 2/22/15.
//
//

#import "CDVLibertyMe.h"

@implementation CDVLibertyMe

- (void)pluginInitialize
{
    [super pluginInitialize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pageDidLoad:) name:CDVPageDidLoadNotification object:self.webView];
    
    // set version
    NSDictionary *dict = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [dict objectForKey:@"CFBundleVersion"];
    NSString *shortVersionString = [dict objectForKey:@"CFBundleShortVersionString"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"v%@(%@)", shortVersionString, version] forKey:@"appVersion"];
    [defaults synchronize];
}

- (void) pageDidLoad:(NSNotification *) notification
{
    if (![[notification name] isEqualToString:@"CDVPageDidLoadNotification"]) {
        return ;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL useStaging = [defaults boolForKey:@"appEnvUseStaging"];
    
    NSString *format = @"window.app = window.app || {};window.app.config = window.app.config || {};window.app.config.useStaging=%@;";
    NSString *js = [NSString stringWithFormat:format, useStaging ? @"true" : @"false"];
    [self.commandDelegate evalJs:js];
}

-(void)clearBadgeNumber:(CDVInvokedUrlCommand *)command
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    [self successWithCallbackID:command.callbackId];
}

- (void)copyToClipboard:(CDVInvokedUrlCommand*)command
{
    if ([command.arguments count] == 0) {
        [self failWithCallbackID:command.callbackId withMessage:@"Content is required."];
        return ;
    }
    
    NSString *content = command.arguments[0];
    [[UIPasteboard generalPasteboard] setString:content];
    [self successWithCallbackID:command.callbackId];
}

- (void)successWithCallbackID:(NSString *)callbackID
{
    [self successWithCallbackID:callbackID withMessage:@"OK"];
}

- (void)successWithCallbackID:(NSString *)callbackID withMessage:(NSString *)message
{
    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];
    [self.commandDelegate sendPluginResult:commandResult callbackId:callbackID];
}

- (void)failWithCallbackID:(NSString *)callbackID withError:(NSError *)error
{
    [self failWithCallbackID:callbackID withMessage:[error localizedDescription]];
}

- (void)failWithCallbackID:(NSString *)callbackID withMessage:(NSString *)message
{
    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:message];
    [self.commandDelegate sendPluginResult:commandResult callbackId:callbackID];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];   // this will remove all notification unless added using addObserverForName:object:queue:usingBlock:
}

@end
