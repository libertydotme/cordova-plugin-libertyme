package xu.li.cordova.libertyme;

import android.app.Activity;
import android.content.Context;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONException;

/**
 * Created by xu.li<AthenaLightenedMyPath@gmail.com> on 2/24/15.
 */
public class LibertyMePlugin extends CordovaPlugin {
    public final String TAG = "LibertyMePlugin";

    @Override
    public boolean execute(String action, CordovaArgs args,
                           CallbackContext callbackContext) throws JSONException {

        Activity activity = cordova.getActivity();

        if ("copyToClipboard".equals(action)) {
            String content = args.getString(0);

            // see http://stackoverflow.com/questions/238284/how-to-copy-text-programmatically-in-my-android-app
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(content);
            } else {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData.newPlainText("Content", content);
                clipboard.setPrimaryClip(clip);
            }

            callbackContext.success();
            return true;
        }

        return super.execute(action, args, callbackContext);
    }
}
